#include <iostream>
#include <vector>

using namespace std;

int main(){
    string str = "Hello World!";
    vector <char> vec(str.begin(), str.end());
    for(auto i : vec){
        cout << i << endl;
    }

    cout << "...and my mom =)" << endl;
    return 0;
}
